// Program to verify a Finnish ID number (HETU)

#include <stdio.h>
#define MAX 10 // number of digits including the check-digit

int i;
char id_number[MAX + 1]; // Adding an element for the null byte

char valid_chars[31] = {'0', '1', '2', '3', '4', '5', '6', '7', '8',
                        '9', 'A', 'B', 'C', 'D', 'E', 'F', 'H', 'J',
                        'K', 'L', 'M', 'N', 'P', 'R', 'S', 'T', 'U',  
                        'V', 'W', 'X', 'Y'};

// Pass user input to array
void get_id_number(void)
{
        printf("Enter ID number (w/o '-' or '+', e.g. 120464126J): ");
        fgets(id_number, MAX + 1, stdin);

        for (i = 0; i < 31; ++i)
                if (id_number[MAX - 1] == valid_chars[i])
                        id_number[MAX - 1] = i;

        for (i = 0; i < MAX - 1; ++i)
                id_number[i] -= '0';
}

// Verify
void verify(void)
{
        int init_number = id_number[0] * 10;

        for (i = 1; i < MAX - 1; ++i) {
                init_number += id_number[i];
                if (i < MAX - 2)
                        init_number *= 10;
        }
        
        if (init_number % 31 == id_number[MAX - 1])
                printf("==> ID number is valid.");
        else
                printf("==> ID number is not valid.");

        printf("\n");
}        

int main(void)
{
        void get_id_number(void), verify(void);

        printf("-{*********************************}-\n");
        printf("  Verify a Finnish ID number (HETU)\n");
        printf("-{*********************************}-\n");

        get_id_number();
        verify();

        return 0;
}

