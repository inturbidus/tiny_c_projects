// Program to verify a number with Luhn's algorithm

#include <stdio.h>
#define MAX 9 // Number of digits including the check-digit

int i;
char number[MAX + 1]; // Adding an element for the null byte

// Pass user input to array
void get_number(void)
{
        printf("Enter number: ");
        fgets(number, MAX + 1, stdin);

        for (i = 0; i < MAX; ++i)
                number[i] -= '0';
}

// Multiply every odd element from right to left by 2
void multiply_by_2(void)
{
        for (i = MAX - 2; i >= 0; i -= 2)
                number[i] *= 2; 
}

// Sum digits
void sum_digits(void)
{
        int first_digit, second_digit;

        for (i = MAX - 2; i >= 0; i -= 2)
                if (number[i] > 9) {
                        second_digit = number[i] % 10;
                        first_digit = number[i] / 10;
                        number[i] = second_digit + first_digit;
                }
}

// Verify
void verify(void)
{
        int sum = 0;

        for (i = 0; i < MAX - 1; ++i)
                sum += number[i];

        if (10 - (sum % 10) == number[MAX - 1])
                printf("=>> Number is valid.");
        else
                printf("=>> Number is not valid.");

        printf("\n");
}        

int main(void)
{
        void get_number(void), multiply_by_2(void), sum_digits(void), verify(void);

        printf("-*===================================*-\n");
        printf("Verify a number with the Luhn algorithm\n");
        printf("-*===================================*-\n");

        get_number();
        multiply_by_2();
        sum_digits();
        verify();

        return 0;
}

