// Program to demonstrate int and char variables

#include <stdio.h>

int i;
char c;
char d;

int main(void)
{
        printf("**********\n");
        printf("Formatting\n");
        printf("**********\n");
        i = 38;
        c = '&';
        d = 38;
        printf("int i = 38; char c = '&'; char d = 38;\n");
        printf("i as dec is %d, i as char is %c.\n", i, i);
        printf("c as dec is %d, c as char is %c.\n", c, c);
        printf("d as dec is %d, d as char is %c.\n", d, d);
        printf("\n");

        printf("=============\n");
        printf("Storage space\n");
        printf("=============\n");
        c = 234;
        printf("char c = 234;\nc as dec is %d, c as char is %c\n", c, c);

        return 0;
}

